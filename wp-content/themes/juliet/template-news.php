<?php
/*Template Name: News*/
get_header();
query_posts(array(
   'post_type' => 'news'
)); ?>
<div class="row two-columns">
    <div class="main-column col-md-9">
 	<h2>Latest News Posts</h2>
	<?php
	if ( have_posts() ) {
	while (have_posts()) : the_post();
		include(locate_template('parts/entry.php'));
	endwhile;
	} else { ?><div class="blog-feed-empty"><p><?php esc_html_e('No posts found.', 'juliet'); ?></p></div><?php } ?>

	 <?php if(get_next_posts_link() || get_previous_posts_link()) { ?>
	    <div class="pagination-blog-feed">
	        <?php if( get_next_posts_link() ) { ?><div class="previous_posts"><?php next_posts_link( esc_html__('Previous Posts', 'juliet') ); ?></div><?php } ?>
	        <?php if( get_previous_posts_link() ) { ?><div class="next_posts"><?php previous_posts_link( esc_html__('Next Posts', 'juliet') ); ?></div><?php } ?>
	    </div>
	    <?php } ?>
	</div>
    <?php get_sidebar(); ?>
</div>
<?php
get_footer();
?>